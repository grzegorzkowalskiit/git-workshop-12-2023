# GITWorkshop

## Git na lokalnym komputerze

### Przywitaj się - szybki start

1. Zainstaluj środowisko do pracy z git `https://git-scm.com/downloads`.
1. Sprawdź czy git działa poprawnie za pomocą komendy `git -v lub git --version` w zależności od wersji git.
1. Stwórz konto w serwisie `https://gitlab.com/` i podaj trenerowi maila, który został wykorzystany do stworzenia konta.
   Pamiętaj o aktywacji konta.
1. Skonfiguruj swojego gita. Dodaj nazwę użytkownika i email.
   np. 
   
   `git config --global user.name "Grzegorz Kowalski"`
   
    `git config --global user.email grzegorzkowalski@yahoo.com`
   
   Pamiętaj o podaniu swoich danych.
1. Sprawdź czy konfiguracja została zapisana poprawnie `git config --list --show-origin`.
1. Pobierz repozytorium `git clone https://gitlab.com/grzegorzkowalskiit/git-workshop-12-2023.git`.
1. Dodaj w folderze pobranego repozytorium plik `<tu wstaw swoje imię>.txt`.
1. W pliku dodaj odpowiedzi na poniższe pytania:
   1. Przedstaw się (imię i nazwisko)
   1. Jaki jest dla ciebie cel szkolenia?
   1. Co aktualnie wiesz o temacie szkolenia?
   1. Jakie wykorzystujesz oprogramowanie związane z tematem szkolenia? (wersja, dostawca, system operacyjny itp.)
   1. Powiedz coś o sobie np. hobby albo zabawna historia (dla chętnych ale rekomenduję :)
1. Wykonaj następujące komendy.
   1. `git add .`
   1. `git commit -m "mój pierwszy commit"`
   1. `git push`
1. Sprawdz czy Twoje zmiany został dodane w repozytorium `https://gitlab.com/grzegorzkowalskiit/git-workshop-12-2023.git`.


### Pierwsze kroki - nowe repozytorium

3. Stwórz projekt o nazwie TestProject i zainicjalizuj w nim nowe repozytorium git.
4. Sprawdź status folderu. Upewnij się, że jesteś w dobrym folderze.

### Operacje na plikach: add i commit 

1. Dodaj plik Hello.txt. W projekcie TestProject.
2. Dodaj zmiany do staging area. 
3. Dodaj commit. Sformatuj swój commit message w VIM.
4. Dodaj kolejny plik tekstowy o dowolnej nazwie. Dodaj mu dowolną treść.
5. Dodaj zmiany do staging area. 
6. Dodaj commit z parametrem -m.
7. Sprawdź historię za pomocą narzędzia gitk.

### Ignorowanie plików

1. Dodaj do repozytorium plik .gitignore.
2. Wykorzystaj poznane narzędzie, żeby stworzyć dobry .gitignore [https://www.toptal.com/developers/gitignore](https://www.toptal.com/developers/gitignore).
3. Dodaj do pliku .gitignore wpis, żeby ignorować wszystkie pliki o rozszerzeniu txt.
4. Dodaj nowy plik ignorowany.txt.
5. Spóbuj dodać ignorowany.txt do stagingu.
6. Sprawdź czy to się udało.
7. Jeśli nie sprawdź jaka komenda powoduje, że plik jest ignorowany.

### Operacje na branchach

1. Wyświetl listę gałęzi 
2. Stwórz nową gałąź `develop`.
3. Przełącz się na nią.
4. Dodaj do gałęzi plik index.html z podstawową treścia:
```
<!doctype html>
<html>
     <head>
          <meta charset="UTF-8" />
          <title>Tytuł strony...</title>
     </head>
     <body>
	 <h1>Tu w przyszłości zbudujemy serwis podróżniczy</h1>
     </body>
</html>
```
5. Otwórz plik w przeglądarce. I sprawdź, czy wyświetla się nagłówek i tytuł strony.
6. Dodaj plik do stagingu.
7. Dodaj commit. 
8. Wyświetl listę gałęzi.
9. Sprawdź historię za pomocą narzędzia gitk.
10. Przełacz się na gałąź `main`.
11. Sprawdź czy plik index.html nadal jest dostępny w przestrzeni roboczej.
12. Wróć na gałąź develop. 
13. Dodaj nowy branch `addStyles` (nie przechodź na nią).    
13. Utwórz nową gałąź `sellTrip` i przejdź na nią.
14. Dodaj zdjęcie wybranej atrakcji turystycznej.
15. Zrób commit.
16. Dodaj opis atrakcji turystycznej. 
17. Zrób commit.
18. Dodaj button "Kup teraz".
19. Zrób commit.
20. Sprawdź historię za pomocą narzędzia gitk.
21. Przejdź na branch `addStyles`.
22. Zmień kolor tekstu na różowy.
23. Zrób commit.
24. Sprawdź efekt w przeglądarce.
25. Sprawdź historię za pomocą narzędzia gitk.
26. Wróć na gałąź `sellTrip`.
27. Przetestuj działanie komendy cherry-pick skopiuj za jej pomocą commit dodający różowy kolor. 
Id commitu sprawdź za pomocą komendy gitk.
28. Wróć na gałąź `addStyle`.
29. Usuń gałąź `addStyle`. 
30. Jeśli się nie udało przełącz się na inną gałąź (przetestuj komendę `git checkout -`) i ponów próbę.
31. Scal zmiany z gałęzi `sellTrip` do gałęzi `develop`.
32. Sprawdź historię za pomocą narzędzia gitk.

### Poruszanie się po historii

1. Sprawdź historię gałezi `sellTrip` za pomocą komendy `git log`.
2. Powtórz działanie z wykorzystaniem wersji skróconej `git log --oneline`.
3. Wypisz 3 ostatnie commity.
4. Wypisz wszystkie commity z ostatnich 15 minut.
5. Za pomocą komendy `git show` wypisz informacje o dwóch wybranych commitach.
6. Wykorzystaj komendę `git diff`, żeby sprawdzić jakie zmiany zaszły między 
bieżącym commitem a dwoma commitami temu w pliku index.html
7. Powtórz ostatnie zadanie z wykorzystaniem komendy `git difftool`.
8. Jeżeli uruchomił Ci się Vimdiff możesz wyjść z niego tak samo jak z VIM.
9. Zainstaluj wygodniejsze narzędzie do porównywania zmian.
10. Skonfiguruj gita, żeby korzystało z nowego narzędzia.

### Nadpisywanie historii

1. Przełącz się na gałąź `develop`.
2. Utwórz i przełącz się na nową gałąź `price`.
3. Dodaj do swojego projektu cenę wycieczki.
4. Zrób commit.
5. Okazało się, że to błędna cena i trzeba pilnie wycofać tą zmianę.
6. Wykorzystaj komendę `git revert`, żeby cofnać zmianę.
7. Sprawdź historię za pomocą narzędzia gitk lub komendy `git log`.
8. Dodaj ponownie cenę wyrażoną w złotówkach.
9. Zób commit.
10. Okazało się, że cena się zgadza ale powinna by wyrażona w dolarach.
11. Wykorzystaj parametr --amend żeby poprawić tą zmianę i dodać do commita.
12. Sprawdź historię za pomocą narzędzia gitk lub komendy `git log`.
13. Za pomocą komendy `rebase` dołącz commity z gąłęzi `price` do gałęzi `develop`.

### Git przechowywanie danych.

1. Sprawdź zawartość plików config i HEAD. 
2. Sprawdź zawartość folderu refs.
3. Sprawdź wagę folderu .git.

## Git, a praca zespołowa w sieci

### Pobranie projektu
1. Załóż konto w serwisie GitLab jeśli jeszcze takiego nie posiadasz.
2. Skonfiguruj klucz SSH ED25519 do pracy z GitLab [Instrukcja](https://docs.gitlab.com/ee/user/ssh.html).

### Git push
1. Stwórz na Gitlab nowy projekt.
2. Pobierz go na dysk. 
3. Dodaj do projektu pliki utworzone w ramach rozdziału `Operacje na branchach`.
4. Wypchnij zmiany na serwer.

## Warsztat
Będziemy stosować konwencję komunikacyjną jak komandosi Navy Seal :). Wywołania jedynka, dwójka, trójka.
Czyli osoba z zespołu o przypisanym numerze wykonuje daną cześć zadania.

### Etap 0
1. Dopisz się jako uczestnik do jednego z zespołów. Link do pliku poda trener.
2. Jedynka otrzymuje zaproszenie do repozytorium zespołu na GitLab.
3. Jedynka zaprasza inne osoby do projektu.
2. Przyjmij zaproszenie do projektu, które otrzymasz na podanego w pliku maila.
3. Zrób clone projektu swojego teamu.
4. Sprawdź czy masz dostęp do odpowiedniej tablicy Jira. 
5. Jedynka wspólnie z prowadzącym konfiguruje projekt swojego zespołu.
6. Dwójka tworzy initial commit dodając do main `index.html` i `.gitignore`. Pamiętaj o wypchnięciu zmian.
7. Trójka pobiera zmiany i na podstawie gałęzi default tworzy gałąź `dev`. Pamiętaj o wypchnięciu zmian.
8. Jedynka w Jira konfiguruje w `/settings/repository` ochronę gałęzi `dev` i w `Push rules` możliwość dodawania
gałęzi tylko z przedrostkiem odpowiednim dla projektu Jira.
9. Zapoznaj się z plikami projektu warsztatu.
10. Jedynka zakłada nowego taska etap_0. 
Proszę utworzyć nową gałąź powiązaną z zadaniem i przypisać dwójkę do tego zadania.
11. Dwójka pobiera listę zmian i przełącza się na nową gałąź założoną do taska nr 2.
W ramach zadania należy dodać `reset css` do projektu i assety graficzne. Odpowiednie zasoby dostępne są w folderze. 
dodaj merge request do gałęzi `dev`.
12. Trójka sprawdza merge request do gałęzi `dev` oraz dodaje aprove i merge.

Kolejne etapy pracy wykonujcie analogicznie. 
Zmiany w każdym etapie muszą doprowadzać do sytuacji, że wygląd aplikacji jest zgodny z projektem.
Starajcie się stosować poznane komendy do poprawy historii i rozwiązywania konfliktów. 
W razie problemów poproście trenera o pomoc. Podobnie, jeśli zrozumienie kodu HTML i CSS będzie problematyczne.
Dbajcie o jakość w projekcie. Poprawne nazewnictwo branchy i poprawny opis commit messages. 
Stosujcie się do poznanych zasad.
Jeśli zrobicie literówki albo inne błędy zmieńcie nazwę brancha, użyjcie parametru amend do poprawienia commita itd.

Wariant dla zespołu 3 osobowego.

### Etap 1
1. Dwójka tworzy taska i branch. 
2. Zmiany na podstawie projektu wprowadza jedynka i tworzy merge request. 
3. Trójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 2
1. Trójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza dwójka i tworzy merge request.
3. Jedynka sprawdza merge request oraz dodaje aprove i merge. 

### Etap 3
1. Jedynka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza dwójka i tworzy merge request.
3. Trójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 4
1. Dwójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza trójka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 5
1. Dwójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza jedynka i tworzy merge request.
3. Trójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 6
1. Trójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza jedynka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 7
1. Jedynka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza dwójka i tworzy merge request.
3. Trójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 8
1. Dwójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza trójka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Przygotujcie wydanie aplikacji
1. Upewnijcie się, że wszystkie zmiany są już dodane na gałęzi `dev`.
2. Dodajcie `release` brancha na podstawie gałęzi `dev`. 
3. Do commitu z releasem dodajcie tag `release_v_1`.

Po każdym etapie należy przejrzeć historię projektu. 
Zespoły mogą kolejno prezentować swoje wyniki pracy i omawiać postępy z prowadzącym warsztaty.
Do komunikacji wewnątrz zespołów wykorzystajmy aplikację Microsoft Teams.

Wariant dla zespołu 4 osobowego.

### Etap 1
1. Dwójka tworzy taska i branch. 
2. Zmiany na podstawie projektu wprowadza jedynka i tworzy merge request. 
3. Trójka i czwórka sprawdza merge request oraz dodaje aprove i merge.

### Etap 2
1. Trójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza czwórka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge. 

### Etap 3
1. Jedynka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza dwójka i tworzy merge request.
3. Trójka i czwórka sprawdza merge request oraz dodaje aprove i merge.

### Etap 4
1. Czwórka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza trójka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 5
1. Dwójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza jedynka i tworzy merge request.
3. Trójka i czwórka sprawdza merge request oraz dodaje aprove i merge.

### Etap 6
1. Trójka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza czwórka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Etap 7
1. Jedynka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza dwójka i tworzy merge request.
3. Trójka i czwórka sprawdza merge request oraz dodaje aprove i merge.

### Etap 8
1. Czwórka tworzy taska i branch.
2. Zmiany na podstawie projektu wprowadza trójka i tworzy merge request.
3. Jedynka i dwójka sprawdza merge request oraz dodaje aprove i merge.

### Przygotujcie wydanie aplikacji
1. Upewnijcie się, że wszystkie zmiany są już dodane na gałęzi `dev`.
2. Dodajcie `release` brancha na podstawie gałęzi `dev`. 
3. Do commitu z releasem dodajcie tak `release_v_1`. 
3. Za pomocą komendy rebase dołaczcie zmiany z brancha `release` do brancha `main`.

Po każdym etapie należy przejrzeć historię projektu. 
Zespoły mogą kolejno prezentować swoje wyniki pracy i omawiać postępy z prowadzącym warsztaty.
Do komunikacji wewnątrz zespołów wykorzystajmy aplikację Microsoft Teams.







